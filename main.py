from typing import Optional
from pydantic import BaseModel, EmailStr, Field
import pydantic

import re  # lets practice some regex alongside

# BaseModel also predefines some methods for us,
# just like dataclass does (e.g. __init__)
# for more info, see:
# https://stackoverflow.com/questions/55183333/how-to-use-an-equivalent-to-post-init-method-with-normal-class#55183451

# lowercase, uppercase, numbers, underscore and dash are allowed
# from 3 to 16 characters long is allowed
username_pattern = re.compile("[a-zA-Z0-9_-]{3,16}")

# why regular password complexity requirements are garbage:
# https://stackoverflow.com/a/48346033
# however we should still ensure some basic level of complexity:
# 1. Password should be at least 10 characters long
password_pattern = re.compile(".{10,}")


# user in a database
class User(BaseModel):
    username: str  # should not contain special characters / spaces
    email: Optional[EmailStr]  # should be a valid email (e.g user@example.com)
    password: str  # this is model that we get from POST request for example,
    # so when we need to write password to a database, remember to always
    # salt it and hash (with hmac or sha3 for example)

    def __if_matches_regex(value: str, regex: re.Pattern[str]) -> str:
        if not regex.fullmatch(value):
            raise ValueError(
                f'Invalid value: "{value}" does not match "{regex.pattern}"'
            )
        return value

    @pydantic.field_validator("username")
    @classmethod
    def validate_username(cls, value):
        return cls.__if_matches_regex(value, username_pattern)

    @pydantic.field_validator("password")
    @classmethod
    def validate_password(cls, value):
        return cls.__if_matches_regex(value, password_pattern)


def main():
    user = User(
        # key=value annotations are a MUST
        username="C0mP1eX_Us3Rname",
        email="user@mail.example.com",
        password="pass1234567",
    )
    print(user.username)


if __name__ == "__main__":
    main()
